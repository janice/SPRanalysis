% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/InputProcessingFunctions.R
\name{select_concentrations}
\alias{select_concentrations}
\title{Select the concentrations to be used in model fitting. The concentrations may be selected by the user in the `Incl. Concentrations`
column in the sample sheet, or the algorithm will determine the 'best' range from the linear part of the average RU vs log concentration plot.}
\usage{
select_concentrations(sample_info, x_vals, y_vals)
}
\arguments{
\item{sample_info}{A tibble. The sample_info with a new column: `NumInclConc`. This is the number of concentrations to fit for
each sample.}

\item{x_vals}{A tibble. The time points for the samples and concentrations chosen for analysis.}

\item{y_vals}{A tibble. The RU values for the samples and concentrations chosen for analysis.}

\item{keep_concentrations}{A vector. Contains the indicies of the Time and RU columns corresponding to the chosen concentrations.}

\item{incl_concentrations_values}{A vector. The value of the included concentrations.}

\item{error_idx}{A vector. Indicies for wells that have encountered an error in the selection process.


select_concentrations <- function(sample_info, x_vals, y_vals)}
}
\value{
A list.
}
\description{
Select the concentrations to be used in model fitting. The concentrations may be selected by the user in the `Incl. Concentrations`
column in the sample sheet, or the algorithm will determine the 'best' range from the linear part of the average RU vs log concentration plot.
}
